//
//  AppDelegate.swift
//  UIHere
//
//  Created by Bruno Dushi on 27.01.20.
//  Copyright © 2020 Bruno Dushi. All rights reserved.
//

import UIKit
import NMAKit

let credentials = (
    appId: "4Okz0DdH4Cn2oCHwhrJI",
    appCode: "Dfv5hnVmlCrWwf2d4kFNuQ",
    licenseKey: "J/AV4j3d4Y1bekfrYTsTFdC0HzFN5luh0zL02N6PZOaRg+vRaCXHmoIy/RMFLbC0bmnqbJD1J/ospQFIlkzDwwCZIuKsMKer54yI0ypNXLqRMmNhl1Q2DlFcQRDxSSP0pBuf9WEZ1h9U3nMc2DS6X1BRuYronaqIBO/Pdm6J2tg643oce40AgTm4+QVmynCwGh9FfNWR5pHp23G/EcIzalDKvd4DBObBrDs8E0BRhz2k0hrr/AOJOPqCMqEEndaO+EztAeITnrkhoJG+nrKq1brNTFofWS7YZz/t7829YHwTB1KsUKiNwL3B3u8I9Wm0u5InAAsIyOpq1uSeCsvaxvM1htmkzZRyGbjEWoTgfKLv89/2mXEAZ6wmeqDQW6nCnSKqi7A7qO6MRmBtKKCNf1saXgzmjRqa7Kn9AhvsdTh5XLmGcXzUDj4oqtF47Pekg6Pz+9H+9/PLIwXx95qN4mT2teQl8jnkRWjxiuraxeYPA9Zyy2I/UCDGqproLWaeasQhn1LmkUGP6YL6QYeI+7fT7Wr3Scx2jbXqHSexqZPl23STVEEO//42ZfnRNcAX7PnzXWorc01RvaIKKkcS6k4mXEzjKiIc3CuRYbDskAM2oG+O3U9ERBxz/+8YnbPoiUnj2vkfzoBGLBvzqJ1bCOU25bRCjUcuo6MqJx31KcM="
)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let error = NMAApplicationContext.setAppId(
            credentials.appId,
            appCode: credentials.appCode,
            licenseKey: credentials.licenseKey)
        
        assert(error == NMAApplicationContextError.none, "Please make sure to set valid HERE credentials.")
        return true
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

