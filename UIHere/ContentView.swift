//
//  ContentView.swift
//  UIHere
//
//  Created by Bruno Dushi on 27.01.20.
//  Copyright © 2020 Bruno Dushi. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HereMapUIViewRepresentable()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HereMapUIViewRepresentable()
    }
}
