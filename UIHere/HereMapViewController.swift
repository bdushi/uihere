//
//  HereMapViewController.swift
//  UIHere
//
//  Created by Bruno Dushi on 28.01.20.
//  Copyright © 2020 Bruno Dushi. All rights reserved.
//

import UIKit
import NMAKit

class HereMapViewController: UIViewController {
    var mapView: NMAMapView!
    
    override func viewDidLoad() {
        mapView = NMAMapView(frame: self.view.frame)
        view.addSubview(mapView)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // view.addSubview(NMAMapView(frame: self.view.frame))
    }
}
