//
//  HereMapUIViewRepresentable.swift
//  UIHere
//
//  Created by Bruno Dushi on 28.01.20.
//  Copyright © 2020 Bruno Dushi. All rights reserved.
//

import SwiftUI
import Foundation

struct HereMapUIViewRepresentable: UIViewControllerRepresentable {
    func makeUIViewController(context: UIViewControllerRepresentableContext<HereMapUIViewRepresentable>) -> HereMapViewController {
        return HereMapViewController()
    }

    func updateUIViewController(_ uiViewController: HereMapViewController, context: UIViewControllerRepresentableContext<HereMapUIViewRepresentable>) {

    }
//    func makeUIViewController(context: Context) -> HereMapViewController {
//        return HereMapViewController()
//    }
//
//    func updateUIViewController(_ uiViewController: HereMapViewController, context: Context) {
//
//    }
}
