//
//  HereMapView.swift
//  UIHere
//
//  Created by Bruno Dushi on 28.01.20.
//  Copyright © 2020 Bruno Dushi. All rights reserved.
//

import SwiftUI
import NMAKit

struct HereMapView: UIViewRepresentable {
    func makeUIView(context: Context) -> NMAMapView {
        return NMAMapView(frame: .zero)
    }

    func updateUIView(_ uiView: NMAMapView, context: Context) {
        
    }
}

struct HereMapView_Previews: PreviewProvider {
    static var previews: some View {
        HereMapView()
    }
}
